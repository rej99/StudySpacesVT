<!doctype html>
<html>
<head>
  <title>Search Results</title>
  <meta name="author" content="Edward Reed">
  <link rel="stylesheet" type="text/css" href="studyspaces.css">
</head>

<style>

table {
  background: lightgrey;
}

td {
    text-align: center;
}
</style>

<body>
  <div class="sidenav">
      <img class="logo" src="sslogo.png" width="125px" height="125px">
      <a href="Homepage.html">Home</a>
      <a href="SessionPage.html">Session</a>
      <a href="SearchPage.html">Search</a>
      <a href="AccountManagement.php">Profile</a>
  </div>
  <div class="content">



 <?php
   session_start();
   require_once("db.php");

   $filter = $_SESSION['select'];
   $where = $_SESSION['entry'];


   switch($filter) {
     case "CRN":
       $sql = "select CourseCRN, sessionDate, sessionST, sessionLocation
               from sessions
               where sessionStatus = 'inactive' AND CourseCRN = $where";
        break;
    case "Session Date":
        $sql = "select CourseCRN, sessionDate, sessionST, sessionLocation
                from sessions
                where sessionStatus = 'inactive' AND sessionDate = '2018-08-01'";

        break;
    case "Start Time":
        $sql = "select CourseCRN, sessionDate, sessionST, sessionLocation
                from sessions
                where sessionStatus = 'inactive' AND sessionST = $where";
        break;
    case "Location":
        $sql = "select CourseCRN, sessionDate, sessionST, sessionLocation
                from sessions
                where sessionStatus = 'inactive' AND sessionLocation = $where";
        break;
    default:
        $sql = "select CourseCRN, sessionDate, sessionST, sessionLocation
                from sessions
                where sessionStatus = 'inactive'";
        break;
   }

  $result = $mydb->query($sql);
   echo "<table id='Results' border=1 width=800px >";
   echo "<thead>";
   echo "<th>Course CRN</th><th>Session Date</th><th>Session Start Time</th><th>Session Location</th>";
   echo "</thead>";

   while($row = mysqli_fetch_array($result)) {
     echo "<tr>";
     echo "<td>".$row["CourseCRN"]."</td><td>".$row["sessionDate"]."</td><td>".$row["sessionST"]."</td><td>".$row["sessionLocation"]."</td>";
     echo "</tr>";
   }
  ?>
  </div>
</body>
</html>
